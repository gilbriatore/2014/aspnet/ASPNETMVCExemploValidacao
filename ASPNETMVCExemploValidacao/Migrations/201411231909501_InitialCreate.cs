namespace ASPNETMVCExemploValidacao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tarefas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DataInicial = c.DateTime(nullable: false),
                        Descricao = c.String(nullable: false),
                        ValorInteiro = c.Int(nullable: false),
                        ValorDecimal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValorDouble = c.Double(nullable: false),
                        Status = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Tarefas");
        }
    }
}
