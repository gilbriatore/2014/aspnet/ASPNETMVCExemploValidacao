﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ASPNETMVCExemploValidacao.Models
{
    public class Tarefa
    {
        public int Id { get; set; }

        [DataType(DataType.Date)]
        public DateTime DataInicial { get; set; }

        [Required]
        [Display(Name="Descrição")]
        public string Descricao { get; set; }

        [Required]
        [Display(Name = "Valor Inteiro")]
        public int ValorInteiro { get; set; }

        [Required]
        [Display(Name = "Valor Decimal")]
        [DataType(DataType.Currency)]
        public Decimal ValorDecimal { get; set; }

        [Required]
        [Display(Name = "Valor Double")]
        [DataType(DataType.Currency)]
        public double ValorDouble { get; set; }

        public string Status { get; set; }

        public Tarefa()
        {
            this.DataInicial = DateTime.Now;
        }

    }
}