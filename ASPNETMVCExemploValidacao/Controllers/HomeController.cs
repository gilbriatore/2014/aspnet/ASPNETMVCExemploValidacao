﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASPNETMVCExemploValidacao.Models;

namespace TarefasCRUD.Controllers
{
    public class HomeController : Controller
    {
        Banco ctx = new Banco();

        public ActionResult Index()
        {
            return View(ctx.Tarefas);
        }

        public ActionResult Details(int id)
        {
            return View(ctx.Tarefas.Find(id));
        }

        public ActionResult Create()
        {
            ViewBag.ListaDeStatus = getListaDeStatus();
            return View(new Tarefa());
        }

        [HttpPost]
        public ActionResult Create(Tarefa tarefa)
        {
            ViewBag.ListaDeStatus = getListaDeStatus();
            try
            {
                ctx.Tarefas.Add(tarefa);
                ctx.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            ViewBag.ListaDeStatus = getListaDeStatus();
            return View(ctx.Tarefas.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(Tarefa tarefa)
        {
            ViewBag.ListaDeStatus = getListaDeStatus();
            try
            {
                ctx.Entry(tarefa).State = System.Data.Entity.EntityState.Modified;
                ctx.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            return View(ctx.Tarefas.Find(id));
        }

        [HttpPost]
        public ActionResult Delete(Tarefa tarefa)
        {
            try
            {
                ctx.Entry(tarefa).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        public SelectList getListaDeStatus()
        {
            return new SelectList(new[]{
               "Em análise",
               "Em execução",
               "Aguardando orientação",
               "Atrasada",
               "Concluída"
            });
        }

        //public List<SelectListItem> getListaDeStatus()
        //{
        //    List<SelectListItem> items = new List<SelectListItem>();
        //    items.Add(new SelectListItem()
        //    {
        //        Text = "Em análise",
        //        Value = "1"
        //    });

        //    items.Add(new SelectListItem()
        //    {
        //        Text = "Em execução",
        //        Value = "2"
        //    });

        //    items.Add(new SelectListItem()
        //    {
        //        Text = "Aguardando orientação",
        //        Value = "3"
        //    });

        //    items.Add(new SelectListItem()
        //    {
        //        Text = "Atrasada",
        //        Value = "4"
        //    });

        //    items.Add(new SelectListItem()
        //    {
        //        Text = "Concluída",
        //        Value = "5"
        //    });

        //    return items;
        //}
    }
}